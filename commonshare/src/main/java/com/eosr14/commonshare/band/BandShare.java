package kr.com.eosr14.commonshare.band;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Shin on 2016-01-11.
 */
public class BandShare {
    private String mText;
    private String mRoute;

    public BandShare() {
    }

    public void setText(String text) {
        mText = text;
    }

    public void setRoute(String route) {
        mRoute = route;
    }

    public void postBand(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getBandUri()));
        context.startActivity(intent);
    }

    private String getBandUri() {
        return String.format("bandapp://create/post?text=%s&route=%s",
                mText == null ? "" : mText,
                mRoute == null ? "" : mRoute
        );
    }
} // end of class BandShare

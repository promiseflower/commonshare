package kr.com.eosr14.commonshare.facebook;

import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by Shin on 2016-01-20.
 */
public class FacebookShare {
    private static FacebookShare mInstance = null;

    private FacebookSdk mFacebookSdk;

    private FacebookShare() {
    }


    private static FacebookShare getmInstance() {
        if (FacebookShare.mInstance == null) {
            synchronized (FacebookShare.class) {
                if (FacebookShare.mInstance == null) {
                    FacebookShare.mInstance = new FacebookShare();
                }
            }
        }

        return FacebookShare.mInstance;
    }

    public void sdkInitialize(Context context) {


        FacebookSdk.sdkInitialize(context);
    }

    public boolean canDialogShow() {
        return ShareDialog.canShow(ShareLinkContent.class);
    }

} // end of class FacebookShare

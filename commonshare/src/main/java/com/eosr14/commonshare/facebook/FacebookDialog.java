package kr.com.eosr14.commonshare.facebook;

import android.app.Activity;
import android.net.Uri;
import android.support.v4.app.Fragment;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by Shin on 2016-01-20.
 */
public class FacebookDialog {
    private String mContentTitle;
    private String mContentDescription;
    private String mContentUrl;
    private String mImageUrl;

    private ShareDialog mShareDialog;
    private ShareLinkContent.Builder mShareLinkContentBuilder;

    private FacebookDialog(Activity activity) {
        mShareDialog = new ShareDialog(activity);
        initialize();
    }

    private FacebookDialog(Fragment fragment) {
        mShareDialog = new ShareDialog(fragment);
        initialize();
    }

    private void initialize() {
        mShareLinkContentBuilder = new ShareLinkContent.Builder();
    }

    public void setContentTitle(String contentTitle) {
        mContentTitle = contentTitle;
    }

    public void setContentDescription(String contentDescription) {
        mContentDescription = contentDescription;
    }

    public void setContentUrl(String contentUrl) {
        mContentUrl = contentUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public void show() {
        if (mContentTitle != null) {
            mShareLinkContentBuilder.setContentTitle(mContentTitle);
        }

        if (mContentDescription != null) {
            mShareLinkContentBuilder.setContentDescription(mContentDescription);
        }

        if (mContentUrl != null) {
            mShareLinkContentBuilder.setContentUrl(Uri.parse(mContentUrl));
        }

        if (mImageUrl != null) {
            mShareLinkContentBuilder.setImageUrl(Uri.parse(mImageUrl));
        }

        mShareDialog.show(mShareLinkContentBuilder.build());
    }

} // end of class FacebookDialog

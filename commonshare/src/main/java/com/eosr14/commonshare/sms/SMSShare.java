package kr.com.eosr14.commonshare.sms;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Shin on 2016-01-21.
 */
public class SMSShare {

    private String mMessage;
    private String mAddress;

    public SMSShare() {
    }

    /**
     * SMS으로 전달할 메시지 설정
     *
     * @param message
     */
    public void setMessage(String message) {
        mMessage = message;
    }

    /**
     * SMS으로 전달할 연락처 설정
     *
     * @param address
     */
    public void setAddress(String address) {
        mAddress = address;
    }

    /**
     * 문자 전송 화면 실행
     * @param context
     */
    public void executeSMS(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (mMessage != null) {
            intent.putExtra("sms_body", mMessage);
        }
        if (mAddress != null) {
            intent.putExtra("address", mAddress);
        }

        intent.setType("vnd.android-dir/mms-sms");
        context.startActivity(intent);
    }
} // end of class SMSShare

package kr.com.eosr14.commonshare.line;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Shin on 2016-01-11.
 */
public class LineShare {
    /**
     * Text 형식의 Line 공유
     */
    public static final String CONTENT_TYPE_TEXT = "text";
    /**
     * Image 형식의 Line 공유
     */
    public static final String CONTENT_TYPE_IMAGE = "image";

    private String mContentType;
    private String mContentKey;

    public LineShare() {
        String lineStr = "line://msg/text/?share_tttest";
        Uri uri = Uri.parse(lineStr);

    }

    /**
     * Line 공유 Type 지정
     *
     * @param contentType 공유 Type {@link LineShare#CONTENT_TYPE_TEXT}, {@link LineShare#CONTENT_TYPE_IMAGE}
     */
    public void setContentType(String contentType) {
        mContentType = contentType;
    }

    /**
     * Line 공유할 Text나 Image 관련 데이터 지정.
     * (ContentType에 따라 다르다)
     * <p>
     * ContentType이 Image 일경우 다음을 참고
     * <p>For Android devices, images will be saved as local files. Please refer to the explanation regarding <CONTENT KEY> below for information on how to set parameters.</p>
     *
     * @param contentKey
     * @see LineShare#setContentType(String)
     */
    public void setContentKey(String contentKey) {
        mContentKey = contentKey;
    }

    /**
     * 라인 공유 함수
     *
     * @param context 실행 시키는 Activity
     */
    public void sendLine(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getLineUri()));
        context.startActivity(intent);
    }

    private String getLineUri() {
        return String.format("line://msg/%s/%s"
                , mContentType == null ? CONTENT_TYPE_TEXT : mContentType
                , mContentKey == null ? "" : mContentKey
        );
    }

} // end of class LineShare
